package com.anchormath

object SimpleGraph {

	type VertexType = String
	type EdgeType = String
	case class Vertex(vType: VertexType, vid:String, properties:Map[String, Any] = Map.empty)
	case class Edge(eType: EdgeType, eid: String, properties:Map[String, Any], from: Vertex, to:Vertex)
}

trait SimpleGraph {

	import SimpleGraph._
	
	def addVertexType(vType:String): VertexType

	def addEdgeType(eType: String): EdgeType

	def addRoot(vType:VertexType, properties : Map[String, Any] = Map.empty): Vertex

	/**
	** @param unique if set to true, check type and properties of vertex and edge, and if exact match exists, will just return the vertex, edge pair
	**/
	def addVertex(vType:VertexType, parent:Vertex, eType: EdgeType, 
		properties : Map[String, Any] = Map.empty, edgeProperties: Map[String, Any] = Map.empty, unique = true): (Vertex, Edge)

	def getVertices(vType: String): Set[Vertex]

	def getEdges(eType: String): Set[Edge]

	def getChildren(parent: Vertex, edgeType:Option[EdgeType] = None): Set[Vertex]

	def getRoot(): Vertex
}